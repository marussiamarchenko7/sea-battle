import React from 'react';
import './App.css';
import Game from "./components/Games/Tic Tac Toe/Game";
import Menu from "./components/Menu";
import Header from "./components/Header";
import {BrowserRouter, Route} from "react-router-dom";
import Main from "./components/Main";

const App = (props) => {
    return (
        <BrowserRouter>
            <Header/>
            <div className="AppStyle">

                <div>
                <Menu/>
                <Route path="/main" component={Main}/>
                    <div id="Game">
                <Route path="/game" component={Game}/>
                    </div>
                </div>
            </div>
        </BrowserRouter>
    )
}

export default App;
