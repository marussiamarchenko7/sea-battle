import React from 'react';
import "../../Components.css";
import Popup from "reactjs-popup";
import '../../../App.css';
import Modal from 'react-modal';


class Game extends React.Component {
    state = {
        modalIsOpen: false,
        makeMove: true
    }


    constructor(props) {
        super(props);
        this.state = {
            squares: Array(9).fill(null), count: 0,
            stringOutput: '',
            colors: [
                {
                    name: "pink",
                    code: "rgba(196, 24, 147, 1)"
                },
                {
                    name: "white",
                    code: "white"
                },
                {
                    name: "blue",
                    code: "#66FCF1"
                }
            ],
            value: "",

        }
        this.winnerLine = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    }

    GetWinner = () => {
        let symbol = (this.state.count % 2 === 0) ? 'x' : 'o';
        for (let i = 0; i < 8; i++) {
            let line = this.winnerLine[i];
            if (this.state.squares[line[0]] === symbol && this.state.squares[line[1]] === symbol && this.state.squares[line[2]] === symbol) {
                this.makeMove = undefined;

                if (symbol === "x") {
                    this.setState({stringOutput: "Winner X! You cannot do any moves)"})
                } else {
                    this.setState({stringOutput: "Winner O! You cannot do any moves)"})

                }
            }
        }
    }

    getStringOutput = () => {
        return this.state.stringOutput
    }

    makeMove = event => {
        let data = event.target.getAttribute('data');
        let currentSquares = this.state.squares;
        if (currentSquares[data] === null) {
            currentSquares[data] = (this.state.count % 2 === 0) ? 'x' : 'o';
            this.setState({count: this.state.count + 1});
            this.setState({squares: currentSquares});
        } else {
            return
        }
        this.GetWinner();

    }

    restartGame = () => {
        this.setState({squares: Array(9).fill(null)});
        this.setState({count: 0});
        this.setState({stringOutput: " "})

    }

    changeColor = (event) => {
        document.getElementById("Game").style.color = event.target.getAttribute('color');
    }

    openModal = () => {
        this.setState({modalIsOpen: true});
    };

    closeModal = () => {
        this.setState({modalIsOpen: false});
    };
    inputChange = (event) => {
        let next = this.state;
        next.value = event.target.value;
        this.setState(next);
    }
    inputClick = () => {
        document.getElementById("Game").style.color = this.state.value;
    }


    render() {
        let data = [...this.state.squares.keys()]
        return (<div>
            <div className="tic-tac-toe">
                {data.map(dataItem => <div className='ttt-grid' onClick={this.makeMove} data={dataItem}
                                           key={dataItem}>{this.state.squares[dataItem]}</div>)}
            </div>
            <div>
                <button className="restart" onClick={this.restartGame}>
                    Play again
                </button>
            </div>
            <div>
                <button className="restart" onClick={this.openModal}>ModalWindow</button>
                <Modal isOpen={this.state.modalIsOpen}
                       onRequestClose={this.closeModal}
                       className="Modal"
                       overlayClassName="modalOverlay">
                    <button className="closeButton" onClick={this.closeModal}>X</button>
                    <div>Hi there ;)</div>
                </Modal>
            </div>
            <div>
                <Popup trigger={<button className="restart"> Change Color</button>}>
                    <div className="popupStyle">
                        <p>You can change color here</p>
                        <div className="buttonFlex">
                            {this.state.colors.map(colorItem =>
                                <button className='buttonChange'
                                        onClick={this.changeColor}
                                        color={colorItem.code}
                                        key={colorItem.name}>{colorItem.name}
                                </button>)}
                        </div>
                        <p>OR</p>
                        <div>
                            <div>
                                <input type='text'
                                       className='input'
                                       placeholder='Write here your color'
                                       ref={ref => ref}
                                       onChange={this.inputChange}
                                       value={this.state.value}/>
                                <span className='span'>
                   <div>
                       <button className='buttonChange' type='button' onClick={this.inputClick}> Go! </button>
                   </div>
                  </span>
                            </div>
                        </div>

                    </div>
                </Popup>
            </div>
            <div id="stringOutPut">
                {this.getStringOutput()}

            </div>
        </div>);
    }
}

export default Game;
