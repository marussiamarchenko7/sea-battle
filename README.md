Technical task:
1. The game will consist of 10 ships. They can stand vertically and horizontally, and there must be one empty cell between the ships, including. Ships cannot be L-shaped.
2. Ships are dragged onto the field and you can edit the direction of the ship's position. If a ship comes into contact with another ship, then it returns to its original location.
3. If the battle is the first, randomly determine who shoots first.
Shot coordinates are transmitted by clicking on the selected cell or entering coordinates from the keyboard.
A hit is marked with a cross, a miss is marked with a dot, a cell where the ship cannot definitely will be marked.
Messages "wounded", "killed / sunk"
4. Artificial intelligence-enemy
- placement of the ship.
- to form the coordinates of the shot, according to the established algorithm for conducting combat.
-when it hits the deck of the ship, continue shooting it until the ship is sunk.
- mark the cells where it makes no sense to shoot and exclude the coordinates of these cells from the matrix of possible shots.


1.HTML layout
- field 10 by 10 (background)
- enemy field
- ship container
- game start button
- Two blocks, upper and lower, for displaying informational messages

2.
- field by two-dimensional matrix (double for loop) (field class)
- drag and drop or write a small snippet function
- static function CreateMatrix array and fills it with zeros, which will serve as hit coordinates
- check for neighboring cells.
- make a check on how many "live" cells the ship has left
